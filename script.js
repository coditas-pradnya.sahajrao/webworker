const worker = new Worker('woeker.js')
let chngBg = document.getElementById("change-background")
let sumel = document.getElementById("sum")
let div = document.getElementById('container-div')
let h3 = document.getElementById("h3")

chngBg.addEventListener('click', changeBackground)
sumel.addEventListener('click', (event) => {
    worker.postMessage('hello')
})

function changeBackground() {
    if (document.body.style.background !== 'green') {
        document.body.style.background = 'green'
    } else {
        document.body.style.background = 'blue'
    }
}



function sum() {

    worker.postMessage("hello,worker")
}

worker.onmessage = function(message) {
    const para = document.createElement("p");
    para.innerHTML = `${message.data}`;
    div.appendChild(para);
    console.log(message)
    console.log(message)
}